using System;
using System.Threading;

namespace Snake
{
    public static class Input
    {
        public static event Action<Direction> directionInput;
        public static void GetInput()
        {
            Thread InputThread = new Thread(() => InputLoop());
            InputThread.Start();

            Clock.Tick += ValidateInput;
        }

        private static Direction dir = Direction.Right;
        private static Direction tickDir = Direction.Right;
        private static ConsoleKeyInfo input;
        private static void InputLoop()
        {
            while (!Game.GameOver)
            {
                Thread.Sleep(1);
                if (Console.KeyAvailable) { input = Console.ReadKey(true); }

                switch (input.KeyChar)
                {
                    case 'w':
                        dir = Direction.Up;
                        break;
                    case 's':
                        dir = Direction.Down;
                        break;
                    case 'a':
                        dir = Direction.Left;
                        break;
                    case 'd':
                        dir = Direction.Right;
                        break;
                }
            }
        }

        private static void ValidateInput()
        {
            switch (dir)
            {
                case Direction.Up:
                    tickDir = tickDir == Direction.Down ? Direction.Down : Direction.Up;
                    break;
                case Direction.Down:
                    tickDir = tickDir == Direction.Up ? Direction.Up : Direction.Down;
                    break;
                case Direction.Left:
                    tickDir = tickDir == Direction.Right ? Direction.Right : Direction.Left;
                    break;
                case Direction.Right:
                    tickDir = tickDir == Direction.Left ? Direction.Left : Direction.Right;
                    break;
            }
            directionInput?.Invoke(tickDir);
        }
    }
}