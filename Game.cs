﻿using System;
using System.Collections.Generic;

namespace Snake
{
    class Game
    {
        public static bool GameOver = false;
        private static Point foodPosition;
        private static Frame _frame = new Frame(40, 20);
        private static Snake _snake = new Snake(20, 10);
        private static void Main(string[] args)
        {
            GameInit();
        }

        private static void GameInit()
        {
            Input.GetInput();
            Clock.Tick += GameLoop;
            Clock.StartGameLoop();


            RenderWall();
            SpawnFood();
        }
        private static void GameLoop()
        {
            _snake.Move(_frame);
            CheckFood();
            CheckDeath();
            if (GameOver) return;
            RenderScene();
        }

        private static void RenderScene()
        {
            _frame.Clear();
            RenderFood();
            RenderWall();
            _snake.Render(_frame);
            _frame.Update();
        }

        private static void SpawnFood()
        {
            Random rand = new Random();
            List<Point> freePoints = _frame.UnusedPixels;
            int foodIndex = rand.Next(freePoints.Count);

            foodPosition = freePoints[foodIndex];
        }

        private static void RenderFood()
        {
            _frame.SetPixel(foodPosition.x, foodPosition.y, '*');
        }
        private static void RenderWall()
        {
            char lowHorWall = '‾';
            char upHorWall = '_';
            char verWall = '|';
            for (int i = 0; i < _frame.Height; i++)
            {
                _frame.SetPixel(0, i, verWall);
                _frame.SetPixel(_frame.Width - 1, i, verWall);
            }
            for (int i = 0; i < _frame.Width; i++)
            {
                _frame.SetPixel(i, 0, upHorWall);
                _frame.SetPixel(i, _frame.Height - 1, lowHorWall);
            }

        }
        private static void CheckFood()
        {
            if (_snake.HeadPosition == foodPosition)
            {
                _snake.Add();
                SpawnFood();
            }
        }
        private static void CheckDeath()
        {
            if (_snake.Intersects())
            {
                Clock.Tick -= GameLoop;
                GameOver = true;
                Console.WriteLine("Game Over.");
            }
        }
    }
}
