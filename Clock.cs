using System;
using System.Threading;

namespace Snake
{
    public static class Clock
    {
        public static event Action Tick;
        public static void StartGameLoop()
        {
            Thread t = new Thread(()=>TickLoop());
            t.Start();
        }

        private static void TickLoop()
        {
            while (!Game.GameOver)
            {
                System.Threading.Thread.Sleep(200);
                Tick?.Invoke();
            }
        }

    }
}