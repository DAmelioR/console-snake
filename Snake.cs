using System.Collections.Generic;

namespace Snake
{
    public enum Direction
    {
        Right,
        Down,
        Left,
        Up
    }

    class Snake
    {
        public Point HeadPosition { get => _snake[0]; }

        private List<Point> _snake = new List<Point>();
        private Direction _dir = Direction.Right;
        private Point _tailPosition = new Point(0, 0);

        private char _snakeBodyChar = '@';

        public Snake(int originX, int originY)
        {
            _snake.Add(new Point(originX, originY));
            Input.directionInput += (dir) => _dir = dir;
        }

        public void Move(Frame frame)
        {
            _tailPosition = _snake[_snake.Count - 1];
            for (int i = _snake.Count - 1; i > 0; i--)
            {
                _snake[i] = _snake[i - 1];
            }
            switch (_dir)
            {
                case Direction.Right:
                    _snake[0] += Point.right;
                    break;

                case Direction.Left:
                    _snake[0] += Point.left;
                    break;

                case Direction.Up:
                    _snake[0] += Point.up;
                    break;

                case Direction.Down:
                    _snake[0] += Point.down;
                    break;
            }
            BorderConditions(frame);
        }
        private void BorderConditions(Frame frame)
        {
            Point p = _snake[0];
            if (_snake[0].x == frame.Width - 1) p.x = 1;
            if (_snake[0].x == 0) p.x = frame.Width - 2;
            if (_snake[0].y == frame.Height - 1) p.y = 1;
            if (_snake[0].y == 0) p.y = frame.Height - 2;
            _snake[0] = p;
        }

        public bool Intersects()
        {
            for (int i = 1; i < _snake.Count; i++)
            {
                if (_snake[0] == _snake[i]) return true;
            }
            return false;
        }
        public void Add()
        {
            _snake.Add(_tailPosition);
        }
        public void Render(Frame frame)
        {
            foreach (Point p in _snake)
            {
                frame.SetPixel(p.x, p.y, _snakeBodyChar);
            }
        }
    }
}