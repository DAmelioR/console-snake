using System;
using System.Collections.Generic;

namespace Snake
{
    public class Frame
    {
        private char[,] _pixels;

        public int Width, Height;
        public char[,] Pixels { get => _pixels; }
        public List<Point> UnusedPixels { get => FreePixels(); }

        public Frame(int larghezza, int altezza)
        {
            _pixels = new char[larghezza, altezza];
            Clear();

            Width = larghezza;
            Height = altezza;
        }

        public void Clear()
        {
            for (int i = 0; i < _pixels.GetLength(0); i++)
            {
                for (int j = 0; j < _pixels.GetLength(1); j++)
                {
                    _pixels[i, j] = ' ';
                }
            }
        }
        public void SetPixel(int x, int y, char c)
        {
            _pixels[x, y] = c;
        }
        public void Update()
        {
            Console.Clear();
            for (int i = 0; i < _pixels.GetLength(1); i++)
            {
                for (int j = 0; j < _pixels.GetLength(0); j++)
                {
                    Console.Write(_pixels[j, i]);
                }
                Console.Write('\n');
            }
        }
        
        private List<Point> FreePixels()
        {
            List<Point> freePoints = new List<Point>();
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    if (_pixels[i, j] == ' ') freePoints.Add(new Point(i, j));
                }
            }
            return freePoints;
        }
    }
}