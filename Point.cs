using System;

namespace Snake
{
    public struct Point
    {
        public int x { get; set; }
        public int y { get; set; }

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static Point up = new Point(0, -1);
        public static Point down = new Point(0, 1);
        public static Point left = new Point(-1, 0);
        public static Point right = new Point(1, 0);

        public static Point operator +(Point a, Point b) => new Point(a.x + b.x, a.y + b.y);
        public static bool operator !=(Point a, Point b) => !(a == b);
        public static bool operator ==(Point a, Point b) => a.x == b.x && a.y == b.y;
        public override bool Equals(object obj) => obj is Point point && x == point.x && y == point.y;
        public override int GetHashCode() => HashCode.Combine(x, y);
    }
}